<?php
//JMC => https://code.tutsplus.com/tutorials/a-guide-to-the-wordpress-theme-customizer-adding-a-new-setting--wp-33180
//CUSTOMIZER COLOR ADDITIONS
//FIRST SET
//SETTING UP THE NEW COLOR CONTROL: SOCIAL LINKS 
function pdog1_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_social_link_color',
        array(
            'default'     => '#000000',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'empty',
            array(
                'label'      => __( 'Social Links', 'pdog-storefront' ),
                'section'    => 'colors',
                'settings'   => 'pdog_social_link_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog1_register_theme_customizer' );
//THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog1_customizer_css() {
    ?>
    <style type="text/css">
       #right-head .social a{ color: <?php echo get_theme_mod( 'pdog_social_link_color' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog1_customizer_css' );


//SETTING UP THE NEW COLOR CONTROL: SOCIAL LINKS HOVER
function pdog2_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_social_link_hover_color',
        array(
            'default'     => '#2f2f2f',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'social_link_hover_color',
            array(
                'label'      => __( 'Social Hover Link Color', 'pdog-storefront' ),
                'section'    => 'colors',
                'settings'   => 'pdog_social_link_hover_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog2_register_theme_customizer' );
//THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog2_customizer_css() {
    ?>
    <style type="text/css">
       #right-head .social a:hover { color: <?php echo get_theme_mod( 'pdog_social_link_hover_color' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog2_customizer_css' );

//SECOND SET
//SETTING UP THE NEW COLOR CONTROL: MAIN NAVIGATION LINKS
function pdog3_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_navigation_link_color',
        array(
            'default'     => '#000000',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'social_link_color',
            array(
                'label'      => __( 'Navigation Link Color', 'pdog-storefront' ),
                'section'    => 'colors',
                'settings'   => 'pdog_navigation_link_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog3_register_theme_customizer' );

//THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog3_customizer_css() {
    ?>
    <style type="text/css">
       .main-navigation ul li a, .widget-area .widget a:not(.button):not(.components-button) { color: <?php echo get_theme_mod( 'pdog_navigation_link_color' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog3_customizer_css' );


//SETTING UP THE NEW COLOR CONTROL: NAVIGATION LINKS HOVER
function pdog4_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_navigation_link_hover_color',
        array(
            'default'     => '#ffffff',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'navigation_link_hover_color',
            array(
                'label'      => __( 'Navigation Hover Link Color', 'pdog-storefront' ),
                'section'    => 'colors',
                'settings'   => 'pdog_navigation_link_hover_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog4_register_theme_customizer' );
//THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog4_customizer_css() {
    ?>
    <style type="text/css">
       .main-navigation ul li a:hover, .widget-area .widget a:not(.button):not(.components-button):hover { color: <?php echo get_theme_mod( 'pdog_navigation_link_hover_color' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog4_customizer_css' );

//THIRD SET
//SETTING UP THE NEW COLOR CONTROL: ANNOUNCEMENT TEXT
function pdog5_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_announcement_color',
        array(
            'default'     => '#2f2f2f',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'announcement_color',
            array(
                'label'      => __( 'Announcement Text', 'pdog-storefront' ),
                'section'    => 'colors',
                'settings'   => 'pdog_announcement_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog5_register_theme_customizer' );
//THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog5_customizer_css() {
    ?>
    <style type="text/css">
       p.header-announcement { color: <?php echo get_theme_mod( 'pdog_announcement_color' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog5_customizer_css' );