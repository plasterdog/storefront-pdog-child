<?php
//JMC - backend-experience

// JMC- unregister widgets selectively
 function unregister_default_widgets() {
     unregister_widget('WP_Widget_Pages');
     unregister_widget('WP_Widget_Calendar');
     unregister_widget('WP_Widget_Categories');
     unregister_widget('WP_Widget_Meta');
     unregister_widget('WP_Widget_Archives');
     unregister_widget('WP_Widget_Recent_Posts');
     unregister_widget('WP_Widget_Recent_Comments');
 }
 add_action('widgets_init', 'unregister_default_widgets', 11);

//* -JMC-Replace WordPress login logo with your own
add_action('login_head', 'b3m_custom_login_logo');
function b3m_custom_login_logo() {
echo '<style type="text/css">
h1 a { background-image:url('.get_stylesheet_directory_uri().'/images/login.png) !important; background-size: 250px 150px !important;height: 150px !important; width: 250px !important; margin-bottom: 0 !important; padding-bottom: 0 !important; }
.login form { margin-top: 10px !important; }
</style>';
}

// JMC changing the logo link https://www.wpbeginner.com/wp-tutorials/how-to-change-the-login-logo-url-in-wordpress/
add_filter( 'login_headerurl', 'custom_loginlogo_url' );
function custom_loginlogo_url($url) {
    return 'https://plasterdog.com';
}

// JMC- custom footer message
function modify_footer_admin () {
  echo 'Themed and configured by plasterdog web design. ';
  echo 'CMS Powered by WordPress';
}
add_filter('admin_footer_text', 'modify_footer_admin');

// JMC Remove WordPress Widgets from Dashboard Area
function remove_wp_dashboard_widgets(){

    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');  // Incoming Links
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');  // Quick Press
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');  // Recent Drafts
    remove_meta_box('dashboard_primary', 'dashboard', 'side');   // WordPress blog (News)
    
}
add_action('wp_dashboard_setup', 'remove_wp_dashboard_widgets');

//Remove  WordPress Welcome Panel
remove_action('welcome_panel', 'wp_welcome_panel');

// JMC - change the standard wordpress greeting
add_action( 'admin_bar_menu', 'wp_admin_bar_my_custom_account_menu', 11 );

function wp_admin_bar_my_custom_account_menu( $wp_admin_bar ) {
$user_id = get_current_user_id();
$current_user = wp_get_current_user();
$profile_url = get_edit_profile_url( $user_id );

if ( 0 != $user_id ) {
/* Add the "My Account" menu */
$avatar = get_avatar( $user_id, 28 );
$howdy = sprintf( __('Start editing, %1$s','plasterdog-hero'), $current_user->display_name );
$class = empty( $avatar ) ? '' : 'with-avatar';

$wp_admin_bar->add_menu( array(
'id' => 'my-account',
'parent' => 'top-secondary',
'title' => $howdy . $avatar,
'href' => $profile_url,
'meta' => array(
'class' => $class,
),
) );

}
}

// JMC - COMBINING BOTH CUSTOM WIDGETS INTO A SINGLE WIDGET
add_action('wp_dashboard_setup', 'my_dashboard_widgets');

function my_dashboard_widgets() {
     global $wp_meta_boxes;
     unset(
          $wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins'],
          $wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary'],
         $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']
     );

add_meta_box( 'dashboard_custom_feed', 'STOREFRONT CHILD THEME:', 'dashboard_custom_feed_output', 'dashboard', 'side', 'high' );
}
function dashboard_custom_feed_output() {
     echo '<div class="rss-widget">';
     echo '<div style="margin:0 0 .5em 0;">
<h2>STOREFRONT CHILD THEME MODIFICATIONS:</h2>
<h3>OVERALL</h3>
<ul style="margin:0 0 1em 2em">
<li>made wider on desktop view</li>
<li>modified typography rules</li>
<li>streamlined back-end</li>
</ul>
<h3>HEADER</h3>
<ul style="margin:0 0 1em 2em">
<li>added announcement field</li>
<li>Added in-theme social media controls</li>
<li>split top section </li>
<li>added color controls for social media, main navigation & announcement line</li>
</ul>
<h3>WIDGETS</h3>
<ul style="margin:0 0 1em 2em">
<li>created new widget region to show on pages</li>
<li>created new widget region to show on posts & post archives</li>
<li>created new widget for footer menu</li>
</ul>
<h3>FOOTER</h3>
<ul style="margin:0 0 1em 2em">
<li>added contact fields</li>
<li>split display of new fields</li>
</ul>
<h3>TEMPLATES</h3>
<ul style="margin:0 0 1em 2em">
<li>new page template - no title</li>
<li>new page template - contained full width</li>
<li>post archive - inserted conditional logic for thumbnails </li>
</ul>
<h3>GUTENBERG CLASSES</h3>
<ul style="margin:0 0 1em 2em">
<li>MODIFIERS FOR TITLES (H1-H6) - WILL MAKE 10% LARGER: bigger</li>
<li>MODIFIERS FOR TITLES (H1-H6)- WILL MAKE 10% SMALLER: smaller</li>
</ul>
<h3>MODIFIERS FOR CONTAINERS - ADDING PADDING TO CONTAINER</h3>
<ul style="margin:0 0 1em 2em">
<li>pad-1 = adds 1em padding on left & right</li>
<li>pad-2 = adds 1em padding on left & right</li>
<li>pad-3 = adds 1em padding on left & right</li>
<li>pad-4 = adds 1em padding on left & right</li>
<li>pad-5 = adds 1em padding on left & right</li>
</ul>
<h3>MODIFIERS FOR CONTAINERS - ADDING MARGIN AROUND CONTAINER</h3>
<ul style="margin:0 0 1em 2em">
<li>margin-1 = adds 1em margin on left & right</li>
<li>margin-2 = adds 1em margin on left & right</li>
<li>margin-3 = adds 1em margin on left & right</li>
<li>margin-4 = adds 1em margin on left & right</li>
<li>margin-5 = adds 1em margin on left & right</li>
</ul>
</div>

 <p style="margin:.5em 0 .5em 0;"><strong>DOCUMENTATION:</strong> For more instructions <a href="http://jeffmcnear.com/wordpress-manuals/storefront-child-manual.pdf" target="_blank">Download the manual</a></p>
 <p style="margin:0 0 .5em 0;">Still have a question? contact Jeff McNear by email: <a href="mailto:jeff@plasterdog.com?subject=Question about the Plasterdog Storefront Child">here</a>. </p>
<hr/>';
     

     echo '</div>';
}


//https://css-tricks.com/snippets/wordpress/apply-custom-css-to-admin-area/
add_action('admin_head', 'dashboard_cleanup');

function dashboard_cleanup() {
  echo '<style>

    .metabox-holder .postbox-container .empty-container {
    border: none transparent;
    height: 10px!important;
    position: relative;
    min-height:10px!important;
}
@media only screen and (min-width: 900px) {
     #dashboard-widgets .postbox-container:first-of-type {
        width:33% !important;
    }  

   #dashboard-widgets .postbox-container:first-of-type .postbox .inside {
    padding: 0 12px 12px;
    line-height: 1.4em;
    font-size: 13px;
}
 
    #dashboard-widgets .postbox-container {
        width:66%!important;        
    }

    #dashboard-widgets .postbox-container .postbox .inside {
    padding: 0 6em 3em 3em;
}
}
  </style>';
}
