<?php
/**
 * The template for displaying full width pages.
 *
 * Template Name: Absolutely Full width
 *
 * @package storefront
 */

get_header('alternate'); ?>

	<div id="primary" class="content-area uncontained"><!-- ADDING IN ANOTHER CLASS TO MAKE THINGS NARROWER AT DESKTOP -->
		<main id="main" class="site-main" role="main">



			<?php
			while ( have_posts() ) :
				the_post();

				 ?>
						<!-- TAKING APART THE INSERTION TO DROP THE TITLE -->

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php the_content();?>
						</article><!-- #post-## -->
			<?php	

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
