<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="col-full top-footer-widget">
			<?php if ( ! dynamic_sidebar( 'top-footer' ) ) : ?>
    		<?php endif; // end sidebar widget area ?>
		</div><!-- ends top footer widget -->

		<div class="col-full">

			<?php
			/**
			 * Functions hooked in to storefront_footer action
			 *
			 * @hooked storefront_footer_widgets - 10
			 * @hooked storefront_credit         - 20
			 */
			do_action( 'storefront_footer' );
			?>

<!-- SUPPLEMENTAL FOOTER FIELDS -->
			<div id="left-foot">
			<p> &copy; <?php $the_year = date("Y"); echo $the_year; ?> | All rights reserved<br/>				
			<?php bloginfo( 'description' ); ?>
			<?php if(get_option('pdog_email') && get_option('pdog_email') != '') {?>
			<br/><a href="mailto:<?php echo get_option('pdog_email') ?>?subject=Website Enquiry"><?php echo get_option('pdog_email') ?></a><?php } ?>	
			</p>		
			</div><!-- ends left foot -->

			<div id="right-foot">
			<?php if(get_option('pdog_phone') && get_option('pdog_phone') != '') {?>
			<?php echo get_option('pdog_phone') ?><br/>  <?php } ?>	
			<?php if(!get_option('pdog_phone')) {?>			<?php }?>

			<?php if(get_option('pdog_phone2') && get_option('pdog_phone2') != '') {?>
			<?php echo get_option('pdog_phone2') ?><br/>  <?php } ?>	
			<?php if(!get_option('pdog_phone')) {?>			<?php }?>


			<?php if(get_option('pdog_address') && get_option('pdog_address') != '') {?>
			<?php echo get_option('pdog_address') ?><br/>  <?php } ?>	
			<?php if(!get_option('pdog_address')) {?>			<?php }?>


			<?php if(get_option('pdog_address2') && get_option('pdog_address2') != '') {?>
			<?php echo get_option('pdog_address2') ?>  <?php } ?>	
			<?php if(!get_option('pdog_address2')) {?>			<?php }?>
			</div><!-- ends right foot -->

		</div><!-- .col-full -->
	</footer><!-- #colophon -->

	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
