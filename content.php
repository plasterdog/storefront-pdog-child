<?php
/**
 * Template used to display post content.
 *
 * @package storefront
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header class="entry-header">
	<h1 class="entry-title"><?php the_title(); ?></h1>
</header>
	<?php the_content(); ?>
</article><!-- #post-## -->
