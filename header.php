<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
<!--ENABLING THE ANALYTICS INSERTION -->	
<?php if(get_option('pdog_analytics') && get_option('pdog_analytics') != '') {?>
		<!-- Global Site Tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo get_option('pdog_analytics') ?>"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', '<?php echo get_option('pdog_analytics') ?>');
		</script>
<?php } ?>	

<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<!--LINKING IN THE FONT AWESOME ICONS -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php do_action( 'storefront_before_site' ); ?>
<div id="page" class="hfeed site">
	<?php do_action( 'storefront_before_header' ); ?>
	<header id="masthead" class="site-header" role="banner" style="<?php storefront_header_styles(); ?>">
<div class="header-container">
<!-- ADDING SOME HEADER SUB-DIVISIONS -->	
<div id="left-head">
<!--- THE ANNOUNCEMENT CONDITIONAL -->
			<?php if(get_option('pdog_announcement') && get_option('pdog_announcement') != '') {?>
			<p class="header-announcement"><?php echo get_option('pdog_announcement') ?></p>  <?php } ?>	
			<?php if(!get_option('pdog_announcement')) {?>	&nbsp;		<?php }?>
<!-- END ANNOUNCEMENT CONDITIONAL -->
</div><!-- ends left head -->

<div id="right-head">
			<div class="social">
				<ul class="top-social-icons">

					<?php if(get_option('pdog_facebook') && get_option('pdog_facebook') != '') {?>
					<li><a href="<?php echo get_option('pdog_facebook') ?>" target="_blank" ><i class="fab fa-facebook-square"></i></a>	</li><?php } ?>

					<?php if(get_option('pdog_linkedin') && get_option('pdog_linkedin') != '') {?>
					<li><a href="<?php echo get_option('pdog_linkedin') ?>" target="_blank"><i class="fab fa-linkedin"></i></a>	</li><?php } ?>	
						
					<?php if(get_option('pdog_twitter') && get_option('pdog_twitter') != '') {?>
					<li><a href="<?php echo get_option('pdog_twitter') ?>" target="_blank"><i class="fab fa-twitter-square"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_instagram') && get_option('pdog_instagram') != '') {?>
					<li><a href="<?php echo get_option('pdog_instagram') ?>" target="_blank"><i class="fab fa-instagram"></i></a>	</li><?php } ?>	
					
					<?php if(get_option('pdog_youtube') && get_option('pdog_youtube') != '') {?>
					<li><a href="<?php echo get_option('pdog_youtube') ?>" target="_blank"><i class="fab fa-youtube"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_vimeo') && get_option('pdog_vimeo') != '') {?>
					<li><a href="<?php echo get_option('pdog_vimeo') ?>" target="_blank"><i class="fab fa-vimeo-square"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_email') && get_option('pdog_email') != '') {?>
					<li><a href="mailto:<?php echo get_option('pdog_email') ?>" target="_blank"><i class="fas fa-envelope"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_meetup') && get_option('pdog_meetup') != '') {?>
					<li><a href="<?php echo get_option('pdog_meetup') ?>" target="_blank"><i class="fab fa-meetup"></i></a>	</li><?php } ?>						
				</ul>
			</div><!-- ends social -->
		</div><!--ends right head -->	

</div><!-- ends header container -->
<div class="clear">
	<div class="beta site-title">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" style="color:#<?php header_textcolor(); ?>;"><?php bloginfo( 'name' ); ?></a>
	</div>	
</div><!-- ends clear -->

		<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_html_e( 'Primary Navigation', 'storefront' ); ?>">
		<button class="menu-toggle" aria-controls="site-navigation" aria-expanded="false"><span><?php echo esc_attr( apply_filters( 'storefront_menu_toggle_text', __( 'Menu', 'storefront' ) ) ); ?></span></button>
			<?php
			wp_nav_menu(
				array(
					'theme_location'  => 'primary',
					'container_class' => 'primary-navigation',
				)
			);

			wp_nav_menu(
				array(
					'theme_location'  => 'handheld',
					'container_class' => 'handheld-navigation',
				)
			);
			?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<?php
	/**
	 * Functions hooked in to storefront_before_content
	 *
	 * @hooked storefront_header_widget_region - 10
	 * @hooked woocommerce_breadcrumb - 10
	 */
	do_action( 'storefront_before_content' );
	?>

	<div id="content" class="site-content" tabindex="-1">
		<div class="col-full">

		<?php
		do_action( 'storefront_content_top' );
