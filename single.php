<?php
/**
 * The template for displaying all single posts.
 *
 * @package storefront
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) :
			the_post(); ?>

			<h1 class="entry-title"><?php the_title();?></h1>
			<?php the_content();?>

		<?php endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<!-- ALTERNATIVE WIDGET REGION -->
<div id="secondary" class="widget-area">
     <?php if ( ! dynamic_sidebar( 'post-sidebar' ) ) : ?>
    <?php endif; // end sidebar widget area ?>
</div>

<?php
get_footer();
