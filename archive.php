<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package storefront
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->

<?php while ( have_posts() ) : the_post(); ?>

<!-- TESTING FOR A THUMBNAIL -->
<?php if ( get_the_post_thumbnail( $post_id ) != '' ) { ?>
		<div class="archive_left_picture">	
		<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'medium' ); ?></a>
		</div><!-- ends left picture -->

		<div class="archive_right_text">
		<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
		</header><!-- .entry-header -->
		<?php the_excerpt(); ?>
		<p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">... read the full article</a></p>
		</div><!-- ends right text -->	
		<div class="clear"><hr/></div>

		
<?php   } else { ?>
<!-- WHEN THERE IS NO THUMBNAIL -->
		<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
		</header><!-- .entry-header -->
		<?php the_excerpt(); ?>
		<p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">... read the full article</a></p>
		<div class="clear"><hr/></div>

 <?php    } ?> <!-- CLOSING THE CONDITION -->

<?php endwhile; ?>
		
		<?php else :

			get_template_part( 'content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<!-- ALTERNATIVE WIDGET REGION -->
<div id="secondary" class="widget-area">
     <?php if ( ! dynamic_sidebar( 'post-sidebar' ) ) : ?>
    <?php endif; // end sidebar widget area ?>
</div>

<?php
get_footer();
