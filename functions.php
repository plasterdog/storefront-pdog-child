<?php

// CONNECTING TO THE STOREFRONT PARENT AS THE CHILD

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
 
    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
 
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}

// KEEPING THIS FILE FROM BEING TOO MESSY BY PULLING IN INC FOLDER FILES

require_once( __DIR__ . '/inc/global-fields.php');
require_once( __DIR__ . '/inc/backend-experience.php');
require_once( __DIR__ . '/inc/customizer-styles.php');

//GETTING RID OF THE BREADCRUMBS https://atlantisthemes.com/remove-storefront-breadcrumb/ - 

add_action( 'init', 'storefront_remove_storefront_breadcrumbs' );

function storefront_remove_storefront_breadcrumbs() {
   remove_action( 'storefront_before_content', 'woocommerce_breadcrumb', 10 );
}

// CREATING SOME NEW WIDGET REGIONS

function plasterdog_hero_widgets_init() {


        register_sidebar( array(
        'name'          => __( 'Regular Page Sidebar', 'plasterdog-hero' ),
        'id'            => 'page-sidebar',
        'description'   => 'This sidebar will show on pages only',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>'
    ) );

        register_sidebar( array(
        'name'          => __( 'Post Sidebar', 'plasterdog-hero' ),
        'id'            => 'post-sidebar',
        'description'   => 'This sidebar will show on posts and post arrays',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>'
    ) );

        register_sidebar( array(
        'name'          => __( 'Top Footer Widget Region', 'plasterdog-hero' ),
        'id'            => 'top-footer',
        'description'   => 'This region is intended for a single level footer menu - everything is centered inside this container',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>'
    ) );

 }
add_action( 'widgets_init', 'plasterdog_hero_widgets_init' );

// REMOVE THE "CATEGORY" PREFIX FROM POST ARCHIVE

add_filter( 'get_the_archive_title', 'replaceCategoryName'); 
   function replaceCategoryName ($title) {

   $title =  single_cat_title( '', false );
   return $title; 
}
//  REMOVES THE ADDITIONAL INFORMATION TAB: https://businessbloomer.com/woocommerce-remove-additional-information-tab/

add_filter( 'woocommerce_product_tabs', 'pdog_remove_product_tabs', 9999 );
function pdog_remove_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] ); 
    return $tabs;
}

// REMOVES THE DEFAULT SORTING DROPDOWN: https://businessbloomer.com/woocommerce-remove-default-sorting-dropdown/

add_action( 'init', 'pdog_remove_default_sorting_storefront' );
  
function pdog_remove_default_sorting_storefront() {
   remove_action( 'woocommerce_after_shop_loop', 'woocommerce_catalog_ordering', 10 );
   remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 10 );
}